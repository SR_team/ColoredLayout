[![pipeline  status](https://gitlab.com/SR_team/ColoredLayout/badges/main/pipeline.svg)](https://gitlab.com/SR_team/ColoredLayout/-/commits/main)

# ColoredLayout

Plugin for OpenRGB to change keyboard leds color on layout changes.

### Supported Platforms

- X11 with Xkb
- KDE Sessions
- Wayland Sessions with WM KWin

[![Watch the video](https://videoapi-muybridge.vimeocdn.com/animated-thumbnails/image/bcab5096-5acc-4f8e-bfef-a29d3ca9ec5d.gif?ClientID=vimeo-core-prod&Date=1613743521&Signature=4390b906835a20ec725aaf712cee1bc3a4031d8b)](https://vimeo.com/512448118)



## BUILD

### Requirements

- CMake 3.14+
- C++20 compiler
- qt5 (core, widgets, gui)
- sdbus-c++
- X11 (optional)

```shell
mkdir build && cd build
cmake -DBUILD_SHARED_LIBS=OFF -DBUILD_STATIC_LIBS=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
strip libColoredLayout.so
```

## Installation

Move file [libColoredLayout.so](https://gitlab.com/SR_team/ColoredLayout/-/jobs/artifacts/main/download\?job\=ColoredLayout) to `$XDG_CONFIG_DIR/SOpenRGB/plugins`. By default `$XDG_CONFIG_DIR` is `~/.config`
