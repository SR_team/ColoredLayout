#include "ColoredLayout.h"

#include <Widgets/MainWidget.h>

ResourceManagerInterface *ColoredLayout::RMPointer = nullptr;
std::atomic_bool ColoredLayout::lockControllers = false;

ColoredLayout::ColoredLayout() {
}

ColoredLayout::~ColoredLayout() {}

OpenRGBPluginInfo ColoredLayout::GetPluginInfo() {
	PInfo.Name = "ColoredLayout";
	PInfo.Description = "Colored keyboard language layout";

	PInfo.Location = OPENRGB_PLUGIN_LOCATION_TOP;

	PInfo.Label = "ColoredLayout";

	return PInfo;
}

unsigned int ColoredLayout::GetPluginAPIVersion() {
	return OPENRGB_PLUGIN_API_VERSION;
}

void ColoredLayout::Load( ResourceManagerInterface *resource_manager_ptr ) {
	RMPointer = resource_manager_ptr;
	ColoredLayout::RMPointer->WaitForDeviceDetection();

	RMPointer->RegisterDetectionStartCallback( &ColoredLayout::DetectionStart, this );
	RMPointer->RegisterDetectionEndCallback( &ColoredLayout::DetectionEnd, this );
}

QWidget *ColoredLayout::GetWidget() {
	widget = new MainWidget();

	return widget;
}

QMenu *ColoredLayout::GetTrayMenu() {
	return nullptr;
}

void ColoredLayout::Unload() {
	widget = nullptr;
}

bool ColoredLayout::isControllersAvailable() {
	return !lockControllers.load();
}

std::vector<RGBController *> &ColoredLayout::getControllers() {
	if ( !isControllersAvailable() ) throw std::runtime_error( "Controllers not available" );
	return RMPointer->GetRGBControllers();
}

SettingsManager *ColoredLayout::getSettingsManager() {
	return RMPointer->GetSettingsManager();
}

void ColoredLayout::DetectionStart( void *ctx ) {
	lockControllers = true;

	auto self = (ColoredLayout *)ctx;
	if ( self->widget ) QMetaObject::invokeMethod( self->widget, "onLostDevices", Qt::QueuedConnection );
}

void ColoredLayout::DetectionEnd( void *ctx ) {
	lockControllers = false;

	auto self = (ColoredLayout *)ctx;
	if ( self->widget ) QMetaObject::invokeMethod( self->widget, "onResetDevices", Qt::QueuedConnection );
}
