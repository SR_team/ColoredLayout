#pragma once

#include "ColoredLayout_global.h"
#include <QObject>
#include <OpenRGBPluginInterface.h>

#include <atomic>

class COLOREDLAYOUT_EXPORT ColoredLayout : public QObject, public OpenRGBPluginInterface {
	Q_OBJECT
	Q_PLUGIN_METADATA( IID OpenRGBPluginInterface_IID )
	Q_INTERFACES( OpenRGBPluginInterface )

public:
	ColoredLayout();
	~ColoredLayout();

	OpenRGBPluginInfo PInfo;
	OpenRGBPluginInfo GetPluginInfo() override;
	unsigned int GetPluginAPIVersion() override;
	void Load( ResourceManagerInterface *resource_manager_ptr ) override;
	QWidget *GetWidget() override;
	QMenu *GetTrayMenu() override;
	void Unload() override;

	static ResourceManagerInterface *RMPointer;

	static bool isControllersAvailable();
	static std::vector<RGBController *> &getControllers();
	static SettingsManager *getSettingsManager();

protected:
	class MainWidget *widget = nullptr;

private:
	static std::atomic_bool lockControllers;

	static void DetectionStart( void * );
	static void DetectionEnd( void * );
};
