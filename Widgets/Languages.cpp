#include "Languages.h"
#include "../ColoredLayout.h"

#include <SettingsManager.h>

Languages::Languages( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
	fallback->setObjectName( "fallback" );
	connect( fallback, &SelectionColor::selected, this, [this]( const QVector<int> &selection, const QColor &color ) {
		presetChanged( fallback->objectName(), selection, color );
	} );
}

Languages::~Languages() {}

void Languages::setWatcher( LayoutWatcher *watcher ) {
	this->watcher = watcher;
	if ( !watcher ) return;
	watcher->onLayoutChanged.append( [this]( std::string_view layout ) { layoutChanged( layout ); } );
	watcher->onLayoutListChanged.append(
		[this]( const std::vector<LayoutWatcher::LayoutNames> &layoutList ) { layoutListChanged( layoutList ); } );
	loadSettings();
	layoutListChanged( watcher->getLayoutsList() );
}

const Languages::preset_t &Languages::getPreset( const QString &name, const QString &serial, const QString &layout ) {
	return presets[name + "/" + serial + "/" + layout];
}

void Languages::setController( RGBController *controller_ptr ) {
	for ( int i = 0; i < langList->count(); ++i ) {
		auto widget = langList->widget( i );
		auto tab = dynamic_cast<SelectionColor *>( widget );
		if ( !tab ) continue;
		tab->setController( controller_ptr );
		if ( controller_ptr ) {
			auto preset = getPreset( controller_ptr->name.c_str(), controller_ptr->serial.c_str(), tab->objectName() );
			if ( preset.selection.empty() && ToRGBColor( preset.color.red(), preset.color.green(), preset.color.blue() ) == 0 )
				preset = getPreset( controller_ptr->name.c_str(), controller_ptr->serial.c_str(), fallback->objectName() );
			tab->selectLeds( preset.selection );
			tab->setColor( preset.color );
		}
	}
	if ( !controller_ptr ) {
		controller_name->setText( "Controller not selected" );
		controller_serial->setText( "" );
		return;
	}
	controller_name->setText( controller_ptr->name.c_str() );
	controller_serial->setText( controller_ptr->serial.c_str() );
}

void Languages::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void Languages::layoutChanged( std::string_view layout ) {
	const auto layoutName = QString::fromUtf8( layout.data(), layout.size() );
	for ( int i = 1; i < langList->count(); ++i ) {
		auto widget = langList->widget( i );
		if ( widget->objectName() != layoutName ) continue;
		langList->setCurrentWidget( widget );
		return;
	}
	auto widget = langList->widget( 0 );
	langList->setCurrentWidget( widget );
}

void Languages::layoutListChanged( const std::vector<LayoutWatcher::LayoutNames> &layoutList ) {
	if ( prevLayouts == layoutList ) return; // жесткий костыль
	prevLayouts = layoutList;

	while ( langList->count() > 1 ) { // Я хз что в ебаном Qt творится, но виджеты остаются, а новые создаются открепленными
		auto widget = langList->widget( 1 );
		if ( widget ) {
			auto addr = "RemoveWidget_" + QString::number( (qlonglong)widget );
			widget->setObjectName( addr );
		}
		langList->removeTab( 1 );
		delete widget;
	}

	for ( auto &&layout : layoutList ) {
		auto tab = new SelectionColor();
		tab->setObjectName( layout.shortName.c_str() );
		connect( tab, &SelectionColor::selected, this, [this, tab]( const QVector<int> &selection, const QColor &color ) {
			presetChanged( tab->objectName(), selection, color );
		} );

		langList->addTab( tab, layout.longName.c_str() );
	}

	if ( watcher ) layoutChanged( watcher->getActiveLayout() );
}

void Languages::presetChanged( const QString &lang, const QVector<int> &selection, const QColor &color ) {
	while ( ColoredLayout::isControllersAvailable() && watcher &&
			presets.size() > ColoredLayout::getControllers().size() * watcher->getLayoutsList().size() ) {
		for ( auto &&[key, preset] : presets ) {
			auto ids = key.split( "/" );
			if ( ids.size() != 3 ) {
				presets.erase( key );
				break;
			}
			auto it = std::find_if( ColoredLayout::getControllers().begin(),
									ColoredLayout::getControllers().end(),
									[&ids]( RGBController *controller ) {
										return ids[0] == controller->name.c_str() && ids[0] == controller->serial.c_str();
									} );
			if ( it == ColoredLayout::getControllers().end() ) {
				presets.erase( key );
				break;
			}
		}
	}
	auto id = controller_name->text() + "/" + controller_serial->text();
	presets[id + "/" + lang] = { selection, color };

	if ( id == "Controller not selected/" ) return;

	auto sm = ColoredLayout::getSettingsManager();
	auto json = json::object();
	try {
		json = sm->GetSettings( "ColoredLayout" );
	} catch ( json::exception &e ) {}
	auto controllers = json::object();
	try {
		controllers = json["Controllers"];
	} catch ( json::exception &e ) {}
	auto controllerSettings = json::object();
	try {
		controllerSettings = controllers[id.toStdString()];
	} catch ( json::exception &e ) {}
	auto lang_ = json::object();

	lang_["leds"] = std::vector<int>( selection.begin(), selection.end() );
	lang_["color"] = ToRGBColor( color.red(), color.green(), color.blue() );

	controllerSettings[lang.toStdString()] = lang_;
	controllers[id.toStdString()] = controllerSettings;
	json["Controllers"] = controllers;
	sm->SetSettings( "ColoredLayout", json );
}

void Languages::loadSettings() {
	if ( !ColoredLayout::isControllersAvailable() || !watcher ) return;
	auto sm = ColoredLayout::getSettingsManager();

	try {
		auto json = sm->GetSettings( "ColoredLayout" )["Controllers"];

		auto loadData = [&]( const std::string &shortName ) {
			for ( auto &&controller : ColoredLayout::getControllers() ) {
				auto id = controller->name + "/" + controller->serial;
				try {
					auto controllerSettings = json[id];
					auto lang = controllerSettings[shortName];
					auto color = lang.value<unsigned int>( "color", 0 );
					auto leds = lang.value<std::vector<int>>( "leds", {} );
					if ( !leds.empty() && color ) {
						auto qvec = QVector<int>( leds.begin(), leds.end() );
						auto qcol = QColor( RGBGetRValue( color ), RGBGetGValue( color ), RGBGetBValue( color ) );
						presets[( id + "/" + shortName ).c_str()] = { qvec, qcol };
					}

				} catch ( json::exception &e ) {}
			}
		};

		for ( auto &&layout : watcher->getLayoutsList() ) { loadData( layout.shortName ); }
		loadData( fallback->objectName().toStdString() );
	} catch ( json::exception &e ) {}
}
