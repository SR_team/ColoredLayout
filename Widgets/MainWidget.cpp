#include "MainWidget.h"
#include "../ColoredLayout.h"

MainWidget::MainWidget( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );

	langs->setWatcher( &watcher );

	connect( devList, &DeviceList::onControllerSelected, this, &MainWidget::controllerSelected );
	watcher.onLayoutChanged.append(
		[this]( std::string_view layout ) { layoutChanged( QString::fromUtf8( layout.data(), layout.size() ) ); } );

	const auto layout = watcher.getActiveLayout();
	layoutChanged( QString::fromUtf8( layout.data(), layout.size() ) );
}

MainWidget::~MainWidget() {
	langs->setWatcher( nullptr );
	devList->saveSettings();
}

void MainWidget::onLostDevices() {
	langs->setController( nullptr );
}

void MainWidget::onResetDevices() {
	devList->updateControllers();
}

void MainWidget::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void MainWidget::controllerSelected( const QString &name, const QString &serial ) {
	if ( !ColoredLayout::isControllersAvailable() ) return;

	for ( auto &&controller : ColoredLayout::getControllers() ) {
		if ( name != controller->name.c_str() ) continue;
		if ( serial != controller->serial.c_str() ) continue;
		langs->setController( controller );
		break;
	}
}

#include <iostream>

void MainWidget::layoutChanged( const QString &layout ) {
	while ( !ColoredLayout::isControllersAvailable() )
		;
	for ( auto &&controller : ColoredLayout::getControllers() ) {
		if ( !devList->isEnabled( controller->name.c_str(), controller->serial.c_str() ) ) continue;
		auto preset = langs->getPreset( controller->name.c_str(), controller->serial.c_str(), layout );
		auto rgbColor = ToRGBColor( preset.color.red(), preset.color.green(), preset.color.blue() );
		if ( rgbColor == 0 ) continue;


		bool changed = false;
		auto color_mode = !controller->modes.empty() ? controller->modes[controller->GetMode()].color_mode : MODE_COLORS_PER_LED;

		if ( color_mode == MODE_COLORS_NONE ) { // FIXME: may be just skip device?
			for ( int i = -1; auto &&mode : controller->modes ) {
				++i;
				if ( mode.color_mode == MODE_COLORS_NONE ) continue;
				controller->SetMode( i );
				break;
			}
		}

		if ( color_mode == MODE_COLORS_PER_LED ) {
			if ( preset.selection.empty() ) continue;

			for ( auto &&b : bakLeds ) {
				if ( controller->GetLED( b.ledId ) == b.color ) continue;
				controller->SetLED( b.ledId, b.color );
				changed = true;
			}
			bakLeds.clear();
			for ( auto &&led : preset.selection ) {
				auto currColor = controller->GetLED( led );
				if ( currColor == rgbColor ) continue;
				bakLeds.emplace_back( led, currColor );
				controller->SetLED( led, rgbColor );
				changed = true;
			}
			if ( changed ) controller->UpdateLEDs();
		} else {
			auto &mode = controller->modes[ controller->GetMode() ];
			if ( mode.colors.empty() ) continue;

			for ( auto &&b : bakLeds ) {
				if ( mode.colors[ b.ledId ] == b.color ) continue;
				mode.colors[ b.ledId ] = b.color;
				changed = true;
			}
			bakLeds.clear();
			for ( int i = -1; auto &color : mode.colors ) {
				++i;
				if ( color == rgbColor ) continue;
				bakLeds.emplace_back( i, color );
				color = rgbColor;
				changed = true;
			}
			if ( changed ) controller->UpdateMode();
		}
	}
}
