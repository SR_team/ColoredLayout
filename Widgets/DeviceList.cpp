#include "DeviceList.h"
#include "../ColoredLayout.h"

#include <SettingsManager.h>
#include <RGBController.h>

DeviceList::DeviceList( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
	auto sm = ColoredLayout::getSettingsManager();
	try {
		auto json = sm->GetSettings( "ColoredLayout" );
		auto checked = json.value<Qt::CheckState>( "OnlyKeyboards", Qt::Unchecked );
		ctrlFilter->setCheckState( checked );
	} catch ( json::exception &e ) {}

	connect( ctrlFilter, &QCheckBox::stateChanged, this, &DeviceList::updateList );
	connect( ctrlList, &QListWidget::currentRowChanged, this, &DeviceList::selectController );

	updateList( ctrlFilter->checkState() );
}

DeviceList::~DeviceList() {}

bool DeviceList::isEnabled( const QString &name, const QString &serial ) {
	for ( int i = 0; i < ctrlList->count(); ++i ) {
		auto item = ctrlList->item( i );
		if ( item->checkState() != Qt::Checked ) continue;
		if ( item->text() != name ) continue;
		if ( item->data( Qt::UserRole ).toString() != serial ) continue;
		return true;
	}
	return false;
}

void DeviceList::updateControllers() {
	if ( !ColoredLayout::isControllersAvailable() ) return;

	updateList( ctrlFilter->checkState() );
}

void DeviceList::saveSettings() {
	auto sm = ColoredLayout::getSettingsManager();
	auto json = json::object();
	try {
		json = sm->GetSettings( "ColoredLayout" );
	} catch ( json::exception &e ) {}

	if ( ctrlList->count() ) {
		auto controllers = json::object();
		try {
			controllers = json["Controllers"];
		} catch ( json::exception &e ) {}

		for ( int i = 0; i < ctrlList->count(); ++i ) {
			auto item = ctrlList->item( i );

			auto controllerSettings = json::object();
			try {
				controllerSettings = controllers[( item->text() + "/" + item->data( Qt::UserRole ).toString() ).toStdString()];
			} catch ( json::exception &e ) {}
			controllerSettings["Checked"] = item->checkState();

			controllers[( item->text() + "/" + item->data( Qt::UserRole ).toString() ).toStdString()] = controllerSettings;
		}
		json["Controllers"] = controllers;
	}

	json["OnlyKeyboards"] = ctrlFilter->checkState();
	sm->SetSettings( "ColoredLayout", json );
}

void DeviceList::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void DeviceList::laodControllers( int filterState ) {
	if ( !ColoredLayout::isControllersAvailable() ) return;

	auto sm = ColoredLayout::getSettingsManager();
	for ( auto &&controller : ColoredLayout::getControllers() ) {
		if ( filterState == Qt::Checked && controller->type != DEVICE_TYPE_KEYBOARD ) continue;

		ctrlList->addItem( controller->name.c_str() );
		auto item = ctrlList->item( ctrlList->count() - 1 );

		item->setData( Qt::UserRole, controller->serial.c_str() );

		try {
			auto json = sm->GetSettings( "ColoredLayout" )["Controllers"];
			auto controllerSettings = json[controller->name + "/" + controller->serial];
			auto checked = controllerSettings.value<Qt::CheckState>( "Checked", Qt::Unchecked );
			item->setCheckState( checked );
		} catch ( json::exception &e ) { item->setCheckState( Qt::Unchecked ); }
	}
	ctrlList->setEnabled( true );
}

void DeviceList::updateList( int filterState ) {
	saveSettings();
	ctrlList->setEnabled( false );
	ctrlList->clear();
	if ( filterState == Qt::PartiallyChecked ) return;

	laodControllers( filterState );
}

void DeviceList::selectController( int row ) {
	if ( !ColoredLayout::isControllersAvailable() ) return;
	if ( row < 0 || row > ctrlList->count() ) return;

	auto item = ctrlList->item( row );
	if ( item->checkState() == Qt::Checked ) emit onControllerSelected( item->text(), item->data( Qt::UserRole ).toString() );
}
