#pragma once

#include "ui_DeviceList.h"

class DeviceList : public QWidget, private Ui::DeviceList {
	Q_OBJECT

public:
	explicit DeviceList( QWidget *parent = nullptr );
	virtual ~DeviceList();

	bool isEnabled( const QString &name, const QString &serial );

public slots:
	void updateControllers();
	void saveSettings();

protected:
	void changeEvent( QEvent *e );
	void laodControllers( int filterState );

protected slots:
	void updateList( int filterState );
	void selectController( int row );

signals:
	void onControllerSelected( const QString &name, const QString &serial );
};
